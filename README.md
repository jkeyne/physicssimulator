# [physicsSimulator](https://gitlab.com/jkeyne/physicsSimulator)

This is an individual project for a class. The idea is to create a basic 2D physics simulation.

# Requirements
* Header files - Code/Box.h Code/Floor.h Code/Force.h Code/Friction.h Code/Gravity.h Code/Normal.h Code/Spring.h Code/CSV.h
* Abstract Class - Force (in Code/Force.h and Code/Force.cpp)
	* pure virtual function - Code/Force.h:11 getMagnitude
* Subclasses - Friction (Code/Friction.h Code/Friction.cpp), Gravity (Code/Gravity.h Code/Gravity.cpp), Normal (Code/Normal.h Code/Normal.cpp), and Spring (Code/Spring.h Code/Spring.cpp) are all subclasses of Force
* Virtual function - Force has a virtual function isContact (Force.h:9) that says if the force is a contact force, and as Gravity is the only non-contact force in this simulation, it overrides it to say false (Gravity.h:14)
* Inheritance - Friction, Gravity, Normal, and Spring inherit angle from Force
* Polymorphism - Box has applyForce that takes a Force pointer so it can take any subclass
* Pointers - Box has a vector of Force pointers
* Static variables - CSV has static variables endrow and SEPERATOR, CSV.h:12,14
* Static functions - Gravity has a static function to return G, Gravity.h:15
* Exceptions - CSV throws an exception if it fails to open a file, CSV.cpp:11
* Templates - CSV has a template for writing any none string value, CSV.cpp:36-40
* File I/O - CSV writes data to a textfile formatted to comply with the csv standarad
* Overloaded << or >> - CSV has << overloaded three times, CSV.h:16,17,19
* One other overloaded operator - Force has a overloaded + operator that will return the combined magnitude of the two forces, Force.h:15

## Extra points
* Recursion - none
* Linked lists/stacks/queues - Box has a linked list to store the forces acting on it
* Binary search trees - none
* Binary file with random access - none
* A friend function not related to the << and >> operator - none
