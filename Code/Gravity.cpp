#include "Gravity.h"
#include <math.h>
double Gravity::G = 6.67e-11;
void Gravity::setBox1(Box newBox){
	box1 = newBox;
}

void Gravity::setBox2(Box newBox){
	box2 = newBox;
}

double Gravity::getMagnitude() {
	return G * ( (box1.getMass() * box2.getMass()) / abs(pow((box1.getX()-box2.getX()),2) - pow((box1.getY()-box2.getY()),2)) );
}
