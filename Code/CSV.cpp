#include "CSV.h"
#include <string>
#include <fstream>

std::string CSV::endrow = "\n";
const std::string CSV::SEPERATOR = ",";

void CSV::open(std::string fName) {
	os.open(fName);
	if (os.fail()) {
		throw OPEN_FAILED();
	}
	os << std::fixed;
}

void CSV::close() {
	os.close();
}

CSV& CSV::operator << (const std::string &val) {
	if (val == endrow)
		os << endrow;
	else 
		os << '"' << val << '"' << SEPERATOR;
	return *this;
}

CSV& CSV::operator << (const char * val) {
	if (val == endrow)
		os << endrow;
	else
		os << '"' << val << '"' << SEPERATOR;
	return *this;
}

template<class T>
CSV& CSV::operator << (const T &val) {
	os << val << SEPERATOR;
	return *this;
}

CSV::CSV(std::string fName) {
	open(fName);
}

CSV::~CSV() {
	close();
}
