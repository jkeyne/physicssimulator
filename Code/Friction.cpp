#include "Friction.h"	
void Friction::setFN(Normal newFN){
	fn = newFN;
	double newAngle = newFN.getAngle() + 180;
	while (newAngle >= 360)
		newAngle-=360;
	setAngle(newAngle);
}

void Friction::setSurface(Floor newSurface){
	surface = newSurface;
}

void Friction::setObject(Box newObject){
	object = newObject;
}

double Friction::getMagnitude() {
	double fk = 0;
	double fs = 0;

	// Calculate kinetic friction
	fk = surface.getUk() * fn.getMagnitude();

	// Calculate static friction
	fs = surface.getUs() * fn.getMagnitude();

	if (fs >= fn.getMagnitude() && object.getVx() == 0 && object.getVy() == 0) {
		return fn.getMagnitude();
	} else {
		return fk;
	}
}
