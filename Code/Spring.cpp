#include <math.h>
#include "Spring.h"
void Spring::setBox(Box newBox){
	box = newBox;
}

void Spring::setFloor(Floor newFloor){
	floor = newFloor;
}

void Spring::setD0(double newD0){
	d0 = newD0;
}

void Spring::setK(double newK){
	k = newK;
}

double Spring::getMagnitude() {
	return k * (sqrt(pow((x-box.getX()),2)-pow((y-box.getY()),2))-d0);
}
