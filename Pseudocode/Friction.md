```cpp
class Friction: Force {
	getMagnitude() {
		double fk = 0;
		double fs = 0;

		// Calculate kinetic friction
		fk = surface.getUk() * fn.getMagnitude();

		// Calculate static friction
		fs = surface.getUs() * fn.getMagnitude();

		check if fs is higher than the object's force along same axis
			return fs;
		else
			return fk;
	}
};
```