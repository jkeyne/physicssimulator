```cpp
class Box {
	applyForce(Force *force) {
		forces.push_back(force);
	}

	simulate(double dt) {
		// net F=ma
		double netFX;
		double netFY;
		for i in forces:
			netFX+=i->getXMagnitude();
			netFY+=i->getYMagnitude();

		ax=netFX/mass;
		ay=netFY/mass;

		// v = \int{a dt}
		// dv = a*dt
		// v ~= \sum{dv}
		double dVx = ax*dt;
		double dVy = ay*dt;
		vx+=dVx;
		vy+=dVy;

		// x = \int{vx dt}
		// dx = vx*dt
		// x ~= \sum{dx}
		double dx = vx*dt;
		double dy = vy*dt;
		x+=dx;
		y+=dy;
	}

	writeToCSV(ostream &os) { // Before calling this, make sure to write a header like "x,y,vx,vy,ax,ay\n", maybe also include your ti, tf, and dt values
		os << x << ',' << y << ',' << vx << ',' << vy << ',' << ax << ',' << ay << 't' << endl;
	}
};
```