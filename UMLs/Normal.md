# Normal: Force
Accessiblilty | Name | Description | Verification
--- | --- | --- | ---
\- | magnitude: double | The magnitude of the normal force | must be greater than 0
\+ | setMagnitude(newMagnitude: double): void | Sets magnitude to newMagnitude | newMagnitude must be greater than 0

## Notes
	* A Normal force must be perpendicular to the object
	* Can only repel
	* Contact force

